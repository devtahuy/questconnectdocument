# Ý tưởng sơ bộ về QuestCon ( 1 game finder dành cho người p2p )

- Bạn A thích game genshin nhưng lại không có thời gian cày cuốc.
- Bạn B thích chơi game và rảnh rỗi nhưng lại không có tiền trang trải.
- Bạn A có nhu cầu nhờ người chơi hộ để mình có thể gacha ra những nhân vật ưa thích nhưng không phải tốn quá nhiều thời gian cày cuốc mà vẫn có thể làm những công việc hằng ngày của mình thoải mái. Nhưng lại không biết tìm ở đâu và ai uy tín và muốn làm điều này với số tiền ít ỏi bằng 1-2 ly cafe.
- Bạn B vừa muốn chơi game mà vừa muốn có ít tiền để tiêu vặt

- Người chơi game hộ cần hình ảnh căn cước công dân, hình ảnh cá nhân, nơi ở thường trú, và 1 khoản cọc trước khi làm việc
- Các hình thức chơi game hộ gồm:
1. Daily task
2. Main quest
3. Optional quest
- Các hình thức vi phạm và nghiêm cấm gồm:
1. Xoá item
2. Nâng cấp item không có sự cho phép
3. Sử dụng tài khoản sai mục đích của người thuê
- Một số lưu ý khi cho thuê:
1. Cho thuê là việc tự nguyện của mỗi người nên nếu có lỗi gì xảy ra hay tự giải quyết
2. Một khi đã cho thuê hãy tự ý thức rằng mình đang giao việc cho 1 một người mà mình có thể tin tưởng, nếu có vấn đề xãy ra hay giải quyết mọi việc giữa 2 người.
3. Hãy là một người văn minh tham gia và xây dựng một cộng đồng trẻ khoẻ và đầy niềm vui
- Mô hình cần các chức năng:
- Đăng ký ( page register)
- Đăng nhập (page login)
- Đăng việc (page post-job)
- Tìm việc (page jobs)
- Nhận việc (page job/)
- Checkin bắt đầu làm, checkout kết quả đã hoàn thành. (page jobs/1/checkin)
- Đánh giá người làm việc, Đánh giá người giao việc (page review)
- Profile mỗi người gồm cccd, facebook, sđt, zalo (page profile)
